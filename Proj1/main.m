//
//  main.m
//  Proj1
//
//  Created by Darek Kulig on 03.06.2015.
//  Copyright (c) 2015 Darek Kulig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
